import { posix } from "node:path";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { services: Services, models: Models, utility: { case: Case } } = api;

	const logDebug = api.log( "hitchy:odem:rest:debug" );
	const logError = api.log( "hitchy:odem:rest:error" );

	return {
		policies() {
			const { config: { model: modelConfig = {} } } = api;
			const CORS = Services.OdemRestCors;

			const urlPrefix = modelConfig.urlPrefix || "/api";
			const modelNames = Object.keys( Models );
			const before = new Map();
			const after = new Map();

			before.set( `ALL ${urlPrefix}`, CORS.getCommonRequestFilter() );
			before.set( `ALL ${posix.resolve( urlPrefix, ".schema" )}`, CORS.getRequestFilterForSchemata() );
			after.set( `ALL ${posix.resolve( urlPrefix, ".schema" )}`, reqNotSupported );

			for ( let i = 0, numNames = modelNames.length; i < numNames; i++ ) {
				const name = modelNames[i];
				const routeName = Case.pascalToKebab( name );
				const model = Models[name] || {};

				before.set( `ALL ${posix.resolve( urlPrefix, routeName )}`, CORS.getRequestFilterForModel( model ) );
				before.set( `ALL ${posix.resolve( urlPrefix, routeName, ".schema" )}`, CORS.getRequestFilterForModelSchema( model ) );
				before.set( `ALL ${posix.resolve( urlPrefix, routeName, ':"uuid.uuid"' )}`, CORS.getRequestFilterForModelItem( model ) );

				after.set( `ALL ${posix.resolve( urlPrefix, routeName )}`, reqNotSupported );
			}

			after.set( `OPTIONS ${urlPrefix}`, CORS.finishPreflight() );

			return { before, after };

			/**
			 * Responds on failure in case of not having handled request before.
			 *
			 * @param {Hitchy.Core.IncomingMessage} req request descriptor
			 * @param {Hitchy.Core.ServerResponse} res response manager
			 * @param {function(Error?):void} next callback to invoke when finished
			 * @returns {void}
			 */
			function reqNotSupported( req, res, next ) {
				if ( !res.headersSent && req.method !== "OPTIONS" ) {
					res.status( 400 ).json( { error: "unsupported request" } );
				}

				next();
			}
		},

		blueprints() {
			const modelNames = Object.keys( Models );
			const routes = new Map();
			const modelConfig = api.config.model || {};
			const urlPrefix = modelConfig.urlPrefix || "/api";
			const convenience = modelConfig.convenience == null ? true : Boolean( modelConfig.convenience );

			addGlobalRoutes( routes, urlPrefix, Models );

			for ( let i = 0, numNames = modelNames.length; i < numNames; i++ ) {
				const name = modelNames[i];
				const routeName = Case.pascalToKebab( name );
				const model = Models[name] || {};

				addRoutesOnModel( routes, urlPrefix, routeName, name, model, convenience );
			}

			routes.set( "HEAD " + posix.resolve( urlPrefix, ":model" ), ( _, res ) => res.status( 404 ).send() );
			routes.set( "DELETE " + posix.resolve( urlPrefix, ":model" ), ( _, res ) => res.status( 404 ).json( { error: "no such collection" } ) );

			return routes;
		},
	};

	/**
	 * Generates JSON response indicating rejection of access.
	 *
	 * @param {Hitchy.Core.ServerResponse} res response manager
	 * @returns {void}
	 */
	function resAccessForbidden( res ) {
		res
			.status( 403 )
			.json( {
				error: "access forbidden",
			} );
	}

	/**
	 * Adds routes handling common requests not related to particular model.
	 *
	 * @param {Map<string,function(IncomingMessage,ServerResponse):Promise>} routes maps
	 *        route patterns into function handling requests matching that pattern
	 * @param {string} urlPrefix common prefix to use on every route regarding any model-related processing
	 * @param {object<string,class<Model>>} models lists all currently available models
	 * @returns {void}
	 */
	function addGlobalRoutes( routes, urlPrefix, models ) {
		const { services: { Model: BaseModel, OdemSchema }, utility: { case: { pascalToKebab } } } = api;

		routes.set( `GET ${posix.resolve( urlPrefix, ".schema" )}`, reqFetchSchemata );

		/**
		 * Handles request for listing schemata of all available models.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @returns {void}
		 */
		async function reqFetchSchemata( req, res ) {
			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, "@hitchy.odem.schema" ) ) {
				resAccessForbidden( res );
				return;
			}

			const modelKeys = Object.keys( models );
			const numModels = modelKeys.length;

			const result = {};

			for ( let i = 0; i < numModels; i++ ) {
				const model = models[modelKeys[i]];

				if ( model.prototype instanceof BaseModel &&
				     OdemSchema.mayBeExposed( req, model ) &&
				     OdemSchema.mayBePromoted( req, model ) ) {
					const slug = pascalToKebab( model.name );

					result[slug] = OdemSchema.extractPublicData( model );
				}
			}

			res.json( result );
		}
	}

	/**
	 * Adds routes handling common requests related to selected model.
	 *
	 * @param {Map<string,function(IncomingMessage,ServerResponse):Promise>} routes maps
	 *        route patterns into function handling requests matching that pattern
	 * @param {string} urlPrefix common prefix to use on every route regarding any model-related processing
	 * @param {string} routeName name of model to be used in path name of request
	 * @param {string} modelName name of model derived from name extracted from path name of request in `routeName`
	 * @param {class<Model>} Model model class
	 * @param {boolean} includeConvenienceRoutes set true to include additional set of routes for controlling all action via GET-requests
	 * @returns {void}
	 */
	function addRoutesOnModel( routes, urlPrefix, routeName, modelName, Model, includeConvenienceRoutes ) {
		const { Model: BaseModel, OdemSchema: Schema, OdemUtilityUuid: { ptnUuid } } = Services;

		const modelUrl = posix.resolve( urlPrefix, routeName );

		const reqBadModel = Model.prototype instanceof BaseModel ? null : ( _, res ) => {
			res.status( 500 ).json( { error: "incomplete discovery of model on server-side, looks like hitchy-plugin-odem issue" } );
		};

		if ( includeConvenienceRoutes ) {
			// implement non-REST-compliant rules to simplify manual control of data via browser
			routes.set( "GET " + posix.resolve( modelUrl, "create" ), reqBadModel || reqCreateItem );
			routes.set( "GET " + posix.resolve( modelUrl, "write", ':"uuid.uuid"' ), reqBadModel || reqModifyItem );
			routes.set( "GET " + posix.resolve( modelUrl, "replace", ':"uuid.uuid"' ), reqBadModel || reqReplaceItem );
			routes.set( "GET " + posix.resolve( modelUrl, "has", ':"uuid.uuid"' ), reqBadModel || reqCheckItem );
			routes.set( "GET " + posix.resolve( modelUrl, "remove", ':"uuid.uuid"' ), reqBadModel || reqRemoveItem );
		}

		routes.set( "GET " + posix.resolve( modelUrl, ".schema" ), reqBadModel || reqFetchSchema );

		// here comes the REST-compliant part
		routes.set( "GET " + posix.resolve( modelUrl ), reqBadModel || reqFetchItems );
		routes.set( "QUERY " + posix.resolve( modelUrl ), reqBadModel || reqFetchItems );
		routes.set( "GET " + posix.resolve( modelUrl, ':"uuid.uuid"' ), reqBadModel || reqFetchItem );

		routes.set( "HEAD " + posix.resolve( modelUrl, ':"uuid.uuid"' ), reqBadModel || reqCheckItem );
		routes.set( "HEAD " + posix.resolve( modelUrl ), reqBadModel || reqSuccess );

		routes.set( "POST " + posix.resolve( modelUrl, ':"uuid.uuid"' ), reqBadModel || reqError( 405, "new entry can not be created with uuid" ) );
		routes.set( "POST " + posix.resolve( modelUrl ), reqBadModel || reqCreateItem );

		routes.set( "PUT " + posix.resolve( modelUrl, ':"uuid.uuid"' ), reqBadModel || reqReplaceItem );
		routes.set( "PUT " + posix.resolve( modelUrl ), reqBadModel || reqError( 405, "PUT is not permitted on collections" ) );
		routes.set( "PATCH " + posix.resolve( modelUrl, ':"uuid.uuid"' ), reqBadModel || reqModifyItem );
		routes.set( "PATCH " + posix.resolve( modelUrl ), reqBadModel || reqError( 405, "PATCH is not permitted on collections" ) );

		routes.set( "DELETE " + posix.resolve( modelUrl, ':"uuid.uuid"' ), reqBadModel || reqRemoveItem );
		routes.set( "DELETE " + posix.resolve( modelUrl ), reqBadModel || reqError( 405, "DELETE is not permitted on collections" ) );


		/**
		 * Responds on success.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @returns {void}
		 */
		function reqSuccess( req, res ) {
			if ( Schema.mayBeExposed( req, Model ) ) {
				res.status( 200 ).send();
			} else {
				res.status( 403 ).send();
			}
		}

		/**
		 * Responds on success.
		 *
		 * @param {int} code HTTP status code to respond with
		 * @param {string} message error message to provide in JSON response body
		 * @returns {function(IncomingMessage, ServerResponse):void} handler responding according to parameters
		 */
		function reqError( code, message ) {
			return ( _, res ) => {
				res.status( code ).json( { error: message } );
			};
		}

		/**
		 * Handles request for fetching schema of selected model.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {void}
		 */
		async function reqFetchSchema( req, res ) {
			logDebug( "got request fetching schema" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.schema` ) ) {
				resAccessForbidden( res );
				return;
			}

			res.json( Schema.extractPublicData( Model ) );
		}

		/**
		 * Handles request for checking whether some selected item of model exists
		 * or not.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqCheckItem( req, res ) {
			logDebug( "got request checking if some item exists" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.check` ) ) {
				resAccessForbidden( res );
				return;
			}

			const { uuid } = req.params;
			if ( !ptnUuid.test( uuid ) ) {
				res.status( 400 ).send();
				return;
			}

			const item = new Model( uuid );

			await item.$exists
				.then( exists => {
					res.status( exists ? 200 : 404 ).send();
				} )
				.catch( error => {
					logError( "checking %s:", routeName, error );
					res.status( 500 ).json( { error: error.message } );
				} );
		}

		/**
		 * Handles request for fetching data of selected item.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise<void>} promises request processed successfully
		 */
		async function reqFetchItem( req, res ) {
			logDebug( "got request fetching some item" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.read` ) ) {
				resAccessForbidden( res );
				return;
			}

			const { uuid } = req.params;
			if ( !ptnUuid.test( uuid ) ) {
				res.status( 400 ).json( { error: "invalid UUID" } );
				return;
			}

			const item = new Model( uuid );

			await item.load()
				.then( loaded => res.json( Schema.filterItem( loaded.toObject( { serialized: true } ), req, Model, "read" ) ) )
				.catch( error => {
					logError( "fetching %s:", routeName, error );

					switch ( error.code ) {
						case "ENOENT" : {
							res.status( 404 ).json( { error: "selected item not found" } );
							break;
						}

						default : {
							res.status( 500 ).json( { error: error.message } );
						}
					}
				} );
		}

		/**
		 * Fetches items of a collection optionally required to match some provided
		 * query.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req incoming request
		 * @param {Hitchy.Core.ServerResponse} res response controller
		 * @returns {Promise} promises response sent
		 */
		async function reqFetchItems( req, res ) {
			logDebug( "got request fetching items" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.list` ) ) {
				resAccessForbidden( res );
				return;
			}

			if ( req.headers["x-list-as-array"] ) {
				res.status( 400 ).json( { error: "fetching items as array is deprecated for security reasons" } );
				return;
			}

			let handler = reqListAll;

			if ( req.query.q || req.query.query || req.method === "QUERY" ) {
				handler = reqListMatches;
			}

			await handler.call( this, req, res );
		}

		/**
		 * Parsing provided query and compiling it for Model.find().
		 *
		 * @param {string} query value of query parameter
		 * @returns {object|undefined} compiled test description for Model.find(), undefined if no valid query was found
		 */
		function parseQuery( query ) {
			const simpleTernary = /^([^:\s]+):between:([^:]+):([^:]+)$/i.exec( query );
			if ( simpleTernary ) {
				const [ , name, lower, upper ] = simpleTernary;

				return { between: { name, lower, upper } };
			}

			const simpleBinary = /^([^:\s]+):([a-z]{2,}):(.*)$/i.exec( query );
			if ( simpleBinary ) {
				const [ , name, operation, value ] = simpleBinary;

				return { [operation.toLowerCase()]: { name, value } };
			}

			const simpleUnary = /^([^:\s]+):((?:not)?null)$/i.exec( query );
			if ( simpleUnary ) {
				const [ , name, operation ] = simpleUnary;

				return { [operation.toLowerCase()]: { name } };
			}

			return undefined;
		}

		/**
		 * Handles request for listing all items of model.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqListMatches( req, res ) {
			logDebug( "got request listing matching items" );

			const {
				q: simpleQuery = "",
				offset = 0,
				limit = Infinity,
				sortBy = null,
				descending = false,
				loadRecords = true,
				count = false
			} = req.query;
			let parsedQuery;

			if ( simpleQuery ) {
				// support old-style simple queries with ?q=<query>
				parsedQuery = parseQuery( simpleQuery );

				if ( !parsedQuery ) {
					res.status( 400 ).json( { error: "invalid query, e.g. use ?q=name:operation:value" } );
					return;
				}
			} else if ( req.query.query ) {
				// support JSON-encoded queries in URL with ?query=<json-query>
				try {
					parsedQuery = JSON.parse( req.query.query );
				} catch {
					res.status( 400 ).json( { error: 'invalid extended query, e.g. use ?query={"operation":{"name":value}}' } );
					return;
				}
			} else if ( req.method === "QUERY" ) {
				// support HTTP QUERY method (which is in draft state, currently)
				const query = await req.fetchBody();

				if ( typeof query === "string" ) {
					try {
						parsedQuery = JSON.parse( query );
					} catch {
						res.status( 400 ).json( { error: "invalid query in QUERY request payload" } );
						return;
					}
				} else if ( query && typeof query === "object" && !Array.isArray( query ) ) {
					parsedQuery = query;
				} else {
					res.status( 400 ).json( { error: "invalid query in QUERY request payload" } );
					return;
				}
			} else {
				res.status( 400 ).json( { error: "missing query" } );
				return;
			}

			const meta = count || req.headers["x-count"] ? {} : null;

			await Model.find( Schema.checkQuery( parsedQuery, req, Model ), { offset, limit, sortBy, sortAscendingly: !descending }, {
				metaCollector: meta,
				loadRecords
			} )
				.then( matches => {
					const result = {
						items: matches.map( item => Schema.filterItem( item.toObject( { serialized: true } ), req, Model, "list" ) ),
					};

					if ( meta ) {
						res.set( "x-count", meta.count );
						result.count = meta.count || 0;
					}

					res.json( result );
				} )
				.catch( error => {
					logError( "querying %s:", routeName, error );

					res.status( 500 ).json( { error: error.message } );
				} );
		}

		/**
		 * Handles request for listing all items of model matching single given
		 * condition.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqListAll( req, res ) {
			logDebug( "got request listing all items" );

			const { offset = 0, limit = Infinity, sortBy = null, descending = false, loadRecords = true, count = false } = req.query;
			const meta = count || req.headers["x-count"] ? {} : null;

			await Model.list( {
				offset,
				limit,
				sortBy,
				sortAscendingly: !descending
			}, { loadRecords, metaCollector: meta } )
				.then( matches => {
					const result = {
						items: matches.map( item => Schema.filterItem( item.toObject( { serialized: true } ), req, Model, "list" ) ),
					};

					if ( meta ) {
						res.set( "x-count", meta.count || 0 );
						result.count = meta.count || 0;
					}

					res.json( result );
				} )
				.catch( error => {
					logError( "listing %s:", routeName, error );

					res.status( 500 ).json( { error: error.message } );
				} );
		}

		/**
		 * Handles request for adding new item.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqCreateItem( req, res ) {
			logDebug( "got request creating item" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.create` ) ) {
				resAccessForbidden( res );
				return;
			}

			const item = new Model();

			await ( req.method === "GET" ? Promise.resolve( req.query ) : req.fetchBody() )
				.then( record => {
					if ( record ) {
						if ( record.uuid ) {
							logDebug( "creating %s:", routeName, "new entry can not be created with uuid" );
							res.status( 400 ).json( { error: "new entry can not be created with uuid" } );
							return undefined;
						}

						const filtered = Schema.filterItem( record, req, Model, "create" );
						const definedProps = Model.schema.props;
						const definedComputed = Model.schema.computed;

						for ( const [ key, value ] of Object.entries( filtered ) ) {
							if ( definedProps[key]?.$type || definedComputed[key] ) {
								item[key] = value;
							}
						}
					}

					return item.save().then( saved => {
						logDebug( "created %s with %s", routeName, saved.uuid );
						res.status( 201 ).json( { uuid: saved.uuid } );
					} );
				} )
				.catch( error => {
					logError( "creating %s:", routeName, error );
					res.status( 500 ).json( { error: error.message } );
				} );
		}

		/**
		 * Handles request for updating properties of a selected item.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqModifyItem( req, res ) {
			logDebug( "got request to modify some item" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.write` ) ) {
				resAccessForbidden( res );
				return;
			}

			const { uuid } = req.params;
			if ( !ptnUuid.test( uuid ) ) {
				res.status( 400 ).json( { error: "invalid UUID" } );
				return;
			}

			const item = new Model( uuid );

			await item.$exists
				.then( exists => {
					if ( !exists ) {
						res.status( 404 ).json( { error: "selected item not found" } );
						return undefined;
					}

					return Promise.all( [
						item.load(),
						req.method === "GET" ? Promise.resolve( req.query ) : req.fetchBody(),
					] )
						.then( ( [ loaded, record ] ) => {
							if ( record ) {
								const filtered = Schema.filterItem( record, req, Model, "write" );
								const definedProps = Model.schema.props;
								const definedComputed = Model.schema.computed;

								for ( const [ key, value ] of Object.entries( filtered ) ) {
									if ( definedProps[key]?.$type || definedComputed[key] ) {
										loaded[key] = value;
									}
								}
							}

							return loaded.save()
								.then( saved => {
									res.json( Schema.filterItem( saved.toObject( { serialized: true } ), req, Model, "read" ) );
								} );
						} );
				} )
				.catch( error => {
					logError( "updating %s:", routeName, error );
					res.status( 500 ).json( { error: error.message } );
				} );
		}


		/**
		 * Handles request for updating properties of a selected item.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqReplaceItem( req, res ) {
			logDebug( "got request replacing some item" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.write` ) ) {
				resAccessForbidden( res );
				return;
			}

			const { uuid } = req.params;
			if ( !ptnUuid.test( uuid ) ) {
				res.status( 400 ).json( { error: "invalid UUID" } );
				return;
			}

			const item = new Model( uuid, { onUnsaved: false } );

			await Promise.all( [ item.$exists, req.method === "GET" ? Promise.resolve( req.query ) : req.fetchBody() ] )
				.then( ( [ exists, record ] ) => {
					return ( exists ? item.load() : Promise.resolve() ).then( () => {
						const propNames = Object.keys( Model.schema.props );
						const numPropNames = propNames.length;

						const computedNames = Object.keys( Model.schema.computed );
						const numComputedNames = computedNames.length;
						const filtered = Schema.filterItem( record, req, Model, "write" );

						// update properties, drop those missing in provided record
						for ( let i = 0; i < numPropNames; i++ ) {
							const propName = propNames[i];
							const value = filtered[propName];

							item[propName] = value ?? null;
						}

						// assign all posted computed properties
						for ( let i = 0; i < numComputedNames; i++ ) {
							const computedName = computedNames[i];
							item[computedName] = filtered[computedName];
						}

						return item.save( { ignoreUnloaded: !exists } );
					} );
				} )
				.then( saved => {
					res.json( Schema.filterItem( saved.toObject( { serialized: true } ), req, Model, "read" ) );
				} )
				.catch( error => {
					logError( "updating %s:", routeName, error );
					res.status( 500 ).json( { error: error.message } );
				} );
		}

		/**
		 * Handles request for removing selected item.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req description of request
		 * @param {Hitchy.Core.ServerResponse} res API for creating response
		 * @returns {Promise|undefined} promises request processed successfully
		 */
		async function reqRemoveItem( req, res ) {
			logDebug( "got request removing some item" );

			if ( !Schema.mayBeExposed( req, Model ) ) {
				res.status( 403 ).json( { error: "access forbidden by model" } );
				return;
			}

			if ( api.plugins.authentication && !await Services.Authorization.mayAccess( req.user, `@hitchy.odem.model.${modelName}.remove` ) ) {
				resAccessForbidden( res );
				return;
			}

			const { uuid } = req.params;
			if ( !ptnUuid.test( uuid ) ) {
				res.status( 400 ).json( { error: "invalid UUID" } );
				return;
			}

			const item = new Model( uuid );

			await item.$exists
				.then( exists => {
					if ( exists ) {
						return item.remove()
							.then( () => res.json( {
								uuid,
								status: "OK",
								action: "remove"
							} ) )
							.catch( error => {
								logError( "removing %s:", routeName, error );
								res.status( 500 ).json( { error: error.message } );
							} );
					}

					logError( "request for removing missing %s ignored", routeName );
					res.status( 404 ).json( { error: "no such entry" } );

					return undefined;
				} );
		}
	}
}
