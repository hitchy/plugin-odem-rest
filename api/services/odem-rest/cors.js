export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const Config = this.config;
	const Services = this.services;

	const CommonlyAcceptedHeaders = [
		"Accept", "Accept-Language", "Authorization", "Content-Language",
		"Content-Type", "If-Match", "If-None-Match", "If-Modified-Since",
		"If-Unmodified-Since", "If-Range", "DNT", "Expect"
	];

	const Accepted = {
		schema: {
			methods: ["GET"],
			headers: CommonlyAcceptedHeaders,
		},
		model: {
			methods: [ "GET", "POST", "HEAD" ],
			headers: CommonlyAcceptedHeaders,
		},
		item: {
			methods: [ "GET", "PUT", "PATCH", "HEAD", "DELETE" ],
			headers: CommonlyAcceptedHeaders,
		},
	};

	/**
	 * Handles CORS-related behaviours.
	 *
	 * @name api.services.OdemRestCors
	 */
	class OdemRestCors {
		/**
		 * Generates function for use as a routing policy filtering CORS-related
		 * aspects of requests without relation to some particular model.
		 *
		 * @returns {HitchyRequestPolicyHandler} generated function suitable for registering as routing policy handler
		 */
		static getCommonRequestFilter() {
			return ( req, res, next ) => {
				const origin = req.headers.origin;

				if ( origin != null ) {
					const { model: { origins } = {} } = Config;

					if ( !origins || ( Array.isArray( origins ) && origins.indexOf( origin ) > -1 ) ) {
						res.setHeader( "Access-Control-Allow-Origin", origin );
						res.setHeader( "Access-Control-Max-Age", 600 );
					}
				}

				next();
			};
		}

		/**
		 * Generates function for use as a routing policy filtering CORS-related
		 * aspects of requests for all models' schemata.
		 *
		 * @returns {HitchyRequestPolicyHandler} generated function suitable for registering as routing policy handler
		 */
		static getRequestFilterForSchemata() {
			return ( req, res, next ) => {
				if ( !res.headersSent ) {
					this.handleMethods( null, null, req, res, Accepted.schema.methods );

					if ( res.hasHeader( "Access-Control-Allow-Origin" ) ) {
						this.handleHeaders( null, null, req, res, Accepted.schema.headers );
					}
				}

				next();
			};
		}

		/**
		 * Generates function for use as a routing policy filtering CORS-related
		 * aspects of requests in scope of provided model.
		 *
		 * @param {class<Model>} model class of particular model
		 * @returns {HitchyRequestPolicyHandler} generated function suitable for registering as routing policy handler
		 */
		static getRequestFilterForModel( model ) {
			return ( req, res, next ) => {
				if ( !res.headersSent ) {
					if ( Services.OdemSchema.mayBeExposed( req, model ) ) {
						this.handleMethods( model, null, req, res, Accepted.model.methods );

						if ( res.hasHeader( "Access-Control-Allow-Origin" ) ) {
							this.handleHeaders( model, null, req, res, Accepted.model.headers );
						}
					} else {
						// revoke any access permission commonly granted before
						res.removeHeader( "Access-Control-Allow-Origin" );
						res.removeHeader( "Access-Control-Allow-Methods" );
						res.removeHeader( "Access-Control-Allow-Headers" );
					}
				}

				next();
			};
		}

		/**
		 * Generates function for use as a routing policy filtering CORS-related
		 * aspects of requests in scope of provided model.
		 *
		 * @param {class<Model>} model class of particular model
		 * @returns {HitchyRequestPolicyHandler} generated function suitable for registering as routing policy handler
		 */
		static getRequestFilterForModelSchema( model ) {
			return ( req, res, next ) => {
				if ( !res.headersSent ) {
					if ( Services.OdemSchema.mayBeExposed( req, model ) ) {
						this.handleMethods( model, null, req, res, Accepted.schema.methods );

						if ( res.hasHeader( "Access-Control-Allow-Origin" ) ) {
							this.handleHeaders( model, null, req, res, Accepted.schema.headers );
						}
					} else {
						// revoke any access permission commonly granted before
						res.removeHeader( "Access-Control-Allow-Origin" );
						res.removeHeader( "Access-Control-Allow-Methods" );
						res.removeHeader( "Access-Control-Allow-Headers" );
					}
				}

				next();
			};
		}

		/**
		 * Generates function for use as a routing policy filtering CORS-related
		 * aspects of requests in scope of provided model.
		 *
		 * @param {class<Model>} model class of particular model
		 * @returns {HitchyRequestPolicyHandler} generated function suitable for registering as routing policy handler
		 */
		static getRequestFilterForModelItem( model ) {
			return ( req, res, next ) => {
				if ( !res.headersSent && req.params.uuid !== ".schema" ) {
					if ( Services.OdemSchema.mayBeExposed( req, model ) ) {
						this.handleMethods( model, req.params.uuid, req, res, Accepted.item.methods );

						if ( res.hasHeader( "Access-Control-Allow-Origin" ) ) {
							this.handleHeaders( model, req.params.uuid, req, res, Accepted.item.headers );
						}
					} else {
						// revoke any access permission commonly granted before
						res.removeHeader( "Access-Control-Allow-Origin" );
						res.removeHeader( "Access-Control-Allow-Methods" );
						res.removeHeader( "Access-Control-Allow-Headers" );
					}
				}

				next();
			};
		}

		/**
		 * Ends preflight requests.
		 *
		 * @returns {HitchyRequestPolicyHandler} generated function suitable for registering as routing policy handler
		 */
		static finishPreflight() {
			return ( req, res, next ) => {
				if ( !res.writableEnded ) {
					res.end();
				}

				next();
			};
		}

		/**
		 * Injects response header describing available request methods for URL
		 * processing selected model and optionally addressed item.
		 *
		 * @param {class<Model>} model implementation of model selected by request URL
		 * @param {string} item UUID of model's item addressed in request URL
		 * @param {HitchyIncomingMessage} req request descriptor
		 * @param {HitchyServerResponse} res response manager
		 * @param {string[]} accepted comma-separated list of methods to accept by default
		 * @returns {void}
		 * @protected
		 */
		static handleMethods( model, item, req, res, accepted ) {
			const methods = req.headers["access-control-request-method"] || "";

			if ( methods.length > 0 && res.hasHeader( "Access-Control-Allow-Origin" ) ) {
				// CORS preflight request
				let filtered = methods
					.trim()
					.split( /[\s,]+/ )
					.filter( method => method && accepted.indexOf( method ) > -1 );

				if ( !filtered.length ) {
					filtered = accepted;
				}

				filtered.sort( ( l, r ) => l.toUpperCase().localeCompare( r.toUpperCase() ) );

				res.setHeader( "Access-Control-Allow-Methods", filtered.join( "," ) );
			} else if ( req.headers.origin == null ) {
				// legacy OPTIONS request for list of supported methods
				accepted.sort( ( l, r ) => l.toUpperCase().localeCompare( r.toUpperCase() ) );

				res.setHeader( "Allow", accepted.join( "," ) );
			}
		}

		/**
		 * Injects response header describing available request headers for URL
		 * processing selected model and optionally addressed item.
		 *
		 * @param {class<Model>} model implementation of model selected by request URL
		 * @param {string} item UUID of model's item addressed in request URL
		 * @param {HitchyIncomingMessage} req request descriptor
		 * @param {HitchyServerResponse} res response manager
		 * @param {string[]} accepted comma-separated list of accepted headers
		 * @returns {void}
		 * @protected
		 */
		static handleHeaders( model, item, req, res, accepted ) {
			const headers = req.headers["access-control-request-headers"] || "";

			if ( headers.length > 0 ) {
				// CORS preflight request
				const _accepted = accepted.map( header => header.toLowerCase() );

				let filtered = headers
					.trim()
					.split( /[\s,]+/ )
					.filter( method => {
						const name = ( method || "" ).toLowerCase();

						if ( name && _accepted.indexOf( name ) > -1 ) {
							return true;
						}

						// TODO add support for configuration listing permitted headers

						return name.startsWith( "x-" );
					} );

				if ( !filtered.length ) {
					filtered = accepted;
				}

				filtered.sort( ( l, r ) => l.toLowerCase().localeCompare( r.toLowerCase() ) );

				res.setHeader( "Access-Control-Allow-Headers", filtered.join( "," ) );
			}
		}
	}

	return OdemRestCors;
}
