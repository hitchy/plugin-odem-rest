export const policies = {
	"ALL /": ( req, res, next ) => {
		// fake authentication on demand
		if ( req.query.user ) {
			req.user = true;
		}

		next();
	},
};
