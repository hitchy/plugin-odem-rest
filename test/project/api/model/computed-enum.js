export default {
	props: {
		stateEnum: {
			type: "number"
		},
	},
	computed: {
		state( caption ) {
			const stateList = [ "created", "prepared", "processing", "finished" ];

			if ( caption == null ) {
				// Read-access:
				return stateList[this.stateEnum] || null;
			}

			// Write-access:
			const n = stateList.indexOf( caption );
			this.stateEnum = n > -1 ? n : null;

			return undefined;
		},
	},
};
