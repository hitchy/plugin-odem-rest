export default {
	props: {
		someDate: {
			type: "date",
		},
	},
	options: {
		promote: "private",
	},
};
