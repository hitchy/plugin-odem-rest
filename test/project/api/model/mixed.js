export default {
	props: {
		myDateProp: {
			type: "date",
		},
		myStringProp: {
			type: "string",
		},
		myIndexedStringProp: {
			type: "string",
			index: true,
		},
		myNullableStringProp: {
			type: "string",
			options: {
				label: "Some Nullable String",
			},
			listed: false,
		},
		myIndexedNullableStringProp: {
			type: "string",
			index: true,
		},
		myIntegerProp: {
			type: "integer",
			options: {
				foo: "bar",
				baz() { return true; },
				hello: {
					foo: {
						baz: [
							1,
							function() { return true; },
							2
						],
						bar: 3
					}
				}
			},
			foo: "bar",
			baz() { return true; },
			hello: {
				foo: {
					baz: [
						1,
						function() { return true; },
						2
					],
					bar: 3
				}
			}
		},
		myNumericProp: {
			type: "number",
		},
		myBooleanProp: {
			type: "boolean",
		},
	},
	options: {
		foo: "bar",
		baz() { return true; },
		hello: {
			foo: {
				baz: [
					1,
					function() { return true; },
					2
				],
				bar: 3
			}
		}
	},
};
