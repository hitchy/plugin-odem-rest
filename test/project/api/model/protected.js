export default {
	props: {
		token: {
			type: "string",
		},
	},
	options: {
		expose: "protected",
	},
};
