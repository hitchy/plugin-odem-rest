export default {
	props: {
		card: {
			required: true
		},
		user: {
			required: true
		},
	},
	options: {
		promote: "protected"
	},
	computed: {},
	methods: {},
	hooks: {},
};
