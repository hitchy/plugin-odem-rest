import { describe, it, before, after, beforeEach, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

const modelFoo = { props: { first: {}, second: { private: true }, third: { protected: true } } };
const authzEnableAllOdem = { "@hitchy.odem": "*" };

const queries = [
	{ get: "/api/foo" },
	{ get: "/api/foo/:uuid" },
	{ head: "/api/foo/:uuid" },
	{ get: "/api/foo/.schema" },
	{ get: "/api/.schema" },
	{ delete: "/api/foo/:uuid" },
];

const authzConfig = ( rules = authzEnableAllOdem ) => ( {
	"config/auth.js": `export const auth = { authorizations: ${JSON.stringify( rules )}};`
} );

describe( "REST API works without authentication as it", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
		options: { dependencies: [ "odm", "odm-provider" ] }, // exclude plugin-auth and its dependencies from being enabled as plugins
		files: {
			"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
		}
	} ) );

	beforeEach( () => {
		const item = new ctx.hitchy.api.models.Foo();
		item.first = "foo";
		item.second = "bar";
		item.third = "baz";

		return item.save().then( () => { ctx.fooId = item.uuid; } );
	} );

	afterEach( () => {
		const item = new ctx.hitchy.api.models.Foo( ctx.fooId );
		return item.remove();
	} );

	it( "always provides access", async() => {
		for ( const query of queries ) {
			for ( const [ method, url ] of Object.entries( query ) ) {
				( await ctx[method]( url.replace( /:uuid/g, ctx.fooId ) ) ).statusCode.should.equal( 200 ); // eslint-disable-line no-await-in-loop
			}
		}
	} );
} );

describe( "In case plugin-auth is found without some configuration, REST API", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
		files: {
			"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
		}
	} ) );

	beforeEach( () => {
		const item = new ctx.hitchy.api.models.Foo();
		item.first = "foo";
		item.second = "bar";
		item.third = "baz";

		return item.save().then( () => { ctx.fooId = item.uuid; } );
	} );

	afterEach( () => {
		const item = new ctx.hitchy.api.models.Foo( ctx.fooId );
		return item.remove();
	} );

	it( "prevents all access", async() => {
		for ( const query of queries ) {
			for ( const [ method, url ] of Object.entries( query ) ) {
				( await ctx[method]( url.replace( /:uuid/g, ctx.fooId ) ) ).statusCode.should.equal( 403 ); // eslint-disable-line no-await-in-loop
			}
		}
	} );
} );

describe( "In case plugin-auth is found and some most permissive authorizations are configured, REST API", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
		files: {
			"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
			...authzConfig(),
		}
	} ) );

	beforeEach( () => {
		const item = new ctx.hitchy.api.models.Foo();
		item.first = "foo";
		item.second = "bar";
		item.third = "baz";

		return item.save().then( () => { ctx.fooId = item.uuid; } );
	} );

	afterEach( () => {
		const item = new ctx.hitchy.api.models.Foo( ctx.fooId );
		return item.remove();
	} );

	it( "always provides access", async() => {
		for ( const query of queries ) {
			for ( const [ method, url ] of Object.entries( query ) ) {
				( await ctx[method]( url.replace( /:uuid/g, ctx.fooId ) ) ).statusCode.should.equal( 200, method + " " + url ); // eslint-disable-line no-await-in-loop
			}
		}
	} );
} );

describe( "In case plugin-auth is found and some more specific authorizations are configured, REST API", () => {
	const ctx = {};

	const create = () => {
		const item = new ctx.hitchy.api.models.Foo();
		item.first = "foo";
		item.second = "bar";
		item.third = "baz";

		return item.save().then( () => { ctx.fooId = item.uuid; } );
	};

	describe( "when denying access on reading all models' schemata it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.foo": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.get( "/api/.schema" ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on reading all models' schemata it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.schema": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.get( "/api/.schema" ) ).statusCode.should.equal( 200 );
		} );
	} );

	describe( "when denying access on reading a model's schema it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.schema": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.get( "/api/foo/.schema" ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on reading a model's schema it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.schema": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.get( "/api/foo/.schema" ) ).statusCode.should.equal( 200 );
		} );
	} );

	describe( "when denying access on reading a model's items it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.list": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.get( "/api/foo" ) ).statusCode.should.equal( 403 );
			( await ctx.get( "/api/foo/" ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on reading a model's items it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.list": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.get( "/api/foo" ) ).statusCode.should.equal( 200 );
			( await ctx.get( "/api/foo/" ) ).statusCode.should.equal( 200 );
		} );
	} );

	describe( "when denying access on reading a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.read": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.get( "/api/foo/" + ctx.fooId ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on reading a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.read": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.get( "/api/foo/" + ctx.fooId ) ).statusCode.should.equal( 200 );
		} );
	} );

	describe( "when denying access on checking a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.check": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.head( "/api/foo/" + ctx.fooId ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on checking a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.check": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.head( "/api/foo/" + ctx.fooId ) ).statusCode.should.equal( 200 );
		} );
	} );

	describe( "when denying access on creating a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.create": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.post( "/api/foo", { first: "bam" } ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on creating a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.create": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.post( "/api/foo", { first: "bam" } ) ).statusCode.should.equal( 201 );
		} );
	} );

	describe( "when denying access on updating a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.write": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.put( "/api/foo/" + ctx.fooId, { first: "bam" } ) ).statusCode.should.equal( 403 );
			( await ctx.patch( "/api/foo/" + ctx.fooId, { first: "bam" } ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on updating a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.write": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.put( "/api/foo/" + ctx.fooId, { first: "bam" } ) ).statusCode.should.equal( 200 );
			( await ctx.patch( "/api/foo/" + ctx.fooId, { first: "bam" } ) ).statusCode.should.equal( 200 );
		} );
	} );

	describe( "when denying access on deleting a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Bar.remove": "*" } ),
			}
		} ) );

		it( "rejects access", async() => {
			await create();

			( await ctx.delete( "/api/foo/" + ctx.fooId ) ).statusCode.should.equal( 403 );
		} );
	} );

	describe( "when permitting access on deleting a model's item it", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( { "@hitchy.odem.model.Foo.remove": "*" } ),
			}
		} ) );

		it( "grants access", async() => {
			await create();

			( await ctx.delete( "/api/foo/" + ctx.fooId ) ).statusCode.should.equal( 200 );
		} );
	} );

	// ------------------------------------------------------------------------

	describe( "when not granting access on a model's protected property it", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( {
					"@hitchy.odem.model.Foo.list": "*",
					"@hitchy.odem.model.Foo.read": "*",
					"@hitchy.odem.model.Foo.write": "*",
					"@hitchy.odem.model.Foo.create": "*",
				} ),
			}
		} ) );

		it( "rejects to filter items by the protected property", async() => {
			await create();

			( await ctx.get( "/api/foo?q=first:eq:foo" ) ).statusCode.should.equal( 200 );
			( await ctx.get( "/api/foo?q=third:neq:foo" ) ).statusCode.should.equal( 403 );
			( await ctx.get( "/api/foo?q=third:eq:foo" ) ).statusCode.should.equal( 403 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { third: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 403 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				and: [
					{ eq: { third: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 403 );
		} );

		it( "does not deliver the protected property in listed items", async() => {
			await create();

			const res = await ctx.get( "/api/foo" );
			res.statusCode.should.equal( 200 );
			res.data.items.should.be.an.Array().with.length( 1 );
			res.data.items[0].should.not.have.property( "third" );
		} );

		it( "does not deliver the protected property on reading an item", async() => {
			await create();

			const res = await ctx.get( "/api/foo/" + ctx.fooId );
			res.statusCode.should.equal( 200 );
			res.data.should.not.have.property( "third" );
		} );

		it( "ignores protected property on creating an item", async() => {
			await create();

			const first = await ctx.post( "/api/foo", { first: "created", third: "bam" } );
			first.statusCode.should.equal( 201 );

			const second = await ctx.post( "/api/foo/", { first: "created", third: "bam" } );
			second.statusCode.should.equal( 201 );

			const readFirst = await ctx.get( "/api/foo/" + first.data.uuid );
			readFirst.statusCode.should.equal( 200 );
			readFirst.data.first.should.equal( "created" );
			( readFirst.data.third === undefined ).should.be.true();

			const readSecond = await ctx.get( "/api/foo/" + second.data.uuid );
			readSecond.statusCode.should.equal( 200 );
			readSecond.data.first.should.equal( "created" );
			( readSecond.data.third === undefined ).should.be.true();
		} );

		it( "ignores protected property on replacing an item", async() => {
			await create();

			const write = await ctx.put( "/api/foo/" + ctx.fooId, { first: "replaced", third: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "replaced" );
			( write.data.third === undefined ).should.be.true();

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "replaced" );
			( read.data.third === undefined ).should.be.true();
		} );

		it( "ignores protected property on patching an item", async() => {
			await create();

			const write = await ctx.patch( "/api/foo/" + ctx.fooId, { first: "patched", third: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "patched" );
			( write.data.third === undefined ).should.be.true();

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "patched" );
			( read.data.third === undefined ).should.be.true();
		} );
	} );

	describe( "when granting access on a model's protected property it", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( {
					"@hitchy.odem.model.Foo.list": "*",
					"@hitchy.odem.model.Foo.read": "*",
					"@hitchy.odem.model.Foo.write": "*",
					"@hitchy.odem.model.Foo.create": "*",
					"@hitchy.odem.model.Foo.property.third.list": "*",
					"@hitchy.odem.model.Foo.property.third.read": "*",
					"@hitchy.odem.model.Foo.property.third.write": "*",
					"@hitchy.odem.model.Foo.property.third.create": "*",
				} ),
			}
		} ) );

		it( "accepts to filter items by the protected property", async() => {
			await create();

			( await ctx.get( "/api/foo?q=first:eq:foo" ) ).statusCode.should.equal( 200 );
			( await ctx.get( "/api/foo?q=third:neq:foo" ) ).statusCode.should.equal( 200 );
			( await ctx.get( "/api/foo?q=third:eq:foo" ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { third: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				and: [
					{ eq: { third: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );
		} );

		it( "delivers the protected property in listed items", async() => {
			await create();

			const res = await ctx.get( "/api/foo" );
			res.statusCode.should.equal( 200 );
			res.data.items.should.be.an.Array().with.length( 1 );
			res.data.items[0].should.have.property( "third" );
		} );

		it( "delivers the protected property on reading an item", async() => {
			await create();

			const res = await ctx.get( "/api/foo/" + ctx.fooId );
			res.statusCode.should.equal( 200 );
			res.data.should.have.property( "third" );
		} );

		it( "considers protected property on creating an item", async() => {
			await create();

			const first = await ctx.post( "/api/foo", { first: "created", third: "bam" } );
			first.statusCode.should.equal( 201 );

			const second = await ctx.post( "/api/foo/", { first: "created", third: "bam" } );
			second.statusCode.should.equal( 201 );

			const readFirst = await ctx.get( "/api/foo/" + first.data.uuid );
			readFirst.statusCode.should.equal( 200 );
			readFirst.data.first.should.equal( "created" );
			readFirst.data.third.should.equal( "bam" );

			const readSecond = await ctx.get( "/api/foo/" + second.data.uuid );
			readSecond.statusCode.should.equal( 200 );
			readSecond.data.first.should.equal( "created" );
			readSecond.data.third.should.equal( "bam" );
		} );

		it( "considers protected property on replacing an item", async() => {
			await create();

			const write = await ctx.put( "/api/foo/" + ctx.fooId, { first: "replaced", third: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "replaced" );
			write.data.third.should.equal( "bam" );

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "replaced" );
			read.data.third.should.equal( "bam" );
		} );

		it( "considers protected property on patching an item", async() => {
			await create();

			const write = await ctx.patch( "/api/foo/" + ctx.fooId, { first: "patched", third: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "patched" );
			write.data.third.should.equal( "bam" );

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "patched" );
			read.data.third.should.equal( "bam" );
		} );
	} );

	describe( "when not granting access on a model's private property it", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( {
					"@hitchy.odem.model.Foo.list": "*",
					"@hitchy.odem.model.Foo.read": "*",
					"@hitchy.odem.model.Foo.write": "*",
					"@hitchy.odem.model.Foo.create": "*",
				} ),
			}
		} ) );

		it( "rejects to filter items by the private property", async() => {
			await create();

			( await ctx.get( "/api/foo?q=first:eq:foo" ) ).statusCode.should.equal( 200 );
			( await ctx.get( "/api/foo?q=second:neq:foo" ) ).statusCode.should.equal( 403 );
			( await ctx.get( "/api/foo?q=second:eq:foo" ) ).statusCode.should.equal( 403 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { second: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 403 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				and: [
					{ eq: { second: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 403 );
		} );

		it( "does not deliver the private property in listed items", async() => {
			await create();

			const res = await ctx.get( "/api/foo" );
			res.statusCode.should.equal( 200 );
			res.data.items.should.be.an.Array().with.length( 1 );
			res.data.items[0].should.not.have.property( "second" );
		} );

		it( "does not deliver the private property on reading an item", async() => {
			await create();

			const res = await ctx.get( "/api/foo/" + ctx.fooId );
			res.statusCode.should.equal( 200 );
			res.data.should.not.have.property( "second" );
		} );

		it( "ignores private property on creating an item", async() => {
			await create();

			const first = await ctx.post( "/api/foo", { first: "created", second: "bam" } );
			first.statusCode.should.equal( 201 );

			const second = await ctx.post( "/api/foo/", { first: "created", second: "bam" } );
			second.statusCode.should.equal( 201 );

			const readFirst = await ctx.get( "/api/foo/" + first.data.uuid );
			readFirst.statusCode.should.equal( 200 );
			readFirst.data.first.should.equal( "created" );
			( readFirst.data.second === undefined ).should.be.true();

			const readSecond = await ctx.get( "/api/foo/" + second.data.uuid );
			readSecond.statusCode.should.equal( 200 );
			readSecond.data.first.should.equal( "created" );
			( readSecond.data.second === undefined ).should.be.true();
		} );

		it( "ignores private property on replacing an item", async() => {
			await create();

			const write = await ctx.put( "/api/foo/" + ctx.fooId, { first: "replaced", second: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "replaced" );
			( write.data.second === undefined ).should.be.true();

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "replaced" );
			( read.data.second === undefined ).should.be.true();
		} );

		it( "ignores private property on patching an item", async() => {
			await create();

			const write = await ctx.patch( "/api/foo/" + ctx.fooId, { first: "patched", second: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "patched" );
			( write.data.second === undefined ).should.be.true();

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "patched" );
			( read.data.second === undefined ).should.be.true();
		} );
	} );

	describe( "when granting access on a model's private property it", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( {
					"@hitchy.odem.model.Foo.list": "*",
					"@hitchy.odem.model.Foo.read": "*",
					"@hitchy.odem.model.Foo.write": "*",
					"@hitchy.odem.model.Foo.create": "*",
					"@hitchy.odem.model.Foo.property.second.list": "*",
					"@hitchy.odem.model.Foo.property.second.read": "*",
					"@hitchy.odem.model.Foo.property.second.write": "*",
					"@hitchy.odem.model.Foo.property.second.create": "*",
				} ),
			}
		} ) );

		it( "still rejects to filter items by the private property", async() => {
			await create();

			( await ctx.get( "/api/foo?q=first:eq:foo" ) ).statusCode.should.equal( 200 );
			( await ctx.get( "/api/foo?q=second:neq:foo" ) ).statusCode.should.equal( 403 );
			( await ctx.get( "/api/foo?q=second:eq:foo" ) ).statusCode.should.equal( 403 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { second: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 403 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				and: [
					{ eq: { second: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 403 );
		} );

		it( "still does not deliver the private property in listed items", async() => {
			await create();

			const res = await ctx.get( "/api/foo" );
			res.statusCode.should.equal( 200 );
			res.data.items.should.be.an.Array().with.length( 1 );
			res.data.items[0].should.not.have.property( "second" );
		} );

		it( "still does not deliver the private property on reading an item", async() => {
			await create();

			const res = await ctx.get( "/api/foo/" + ctx.fooId );
			res.statusCode.should.equal( 200 );
			res.data.should.not.have.property( "second" );
		} );

		it( "still ignores private property on creating an item", async() => {
			await create();

			const first = await ctx.post( "/api/foo", { first: "created", second: "bam" } );
			first.statusCode.should.equal( 201 );

			const second = await ctx.post( "/api/foo/", { first: "created", second: "bam" } );
			second.statusCode.should.equal( 201 );

			const readFirst = await ctx.get( "/api/foo/" + first.data.uuid );
			readFirst.statusCode.should.equal( 200 );
			readFirst.data.first.should.equal( "created" );
			( readFirst.data.second === undefined ).should.be.true();

			const readSecond = await ctx.get( "/api/foo/" + second.data.uuid );
			readSecond.statusCode.should.equal( 200 );
			readSecond.data.first.should.equal( "created" );
			( readSecond.data.second === undefined ).should.be.true();
		} );

		it( "still ignores private property on replacing an item", async() => {
			await create();

			const write = await ctx.put( "/api/foo/" + ctx.fooId, { first: "replaced", second: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "replaced" );
			( write.data.second === undefined ).should.be.true();

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "replaced" );
			( read.data.second === undefined ).should.be.true();
		} );

		it( "still ignores private property on patching an item", async() => {
			await create();

			const write = await ctx.patch( "/api/foo/" + ctx.fooId, { first: "patched", second: "bam" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "patched" );
			( write.data.second === undefined ).should.be.true();

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "patched" );
			( read.data.second === undefined ).should.be.true();
		} );
	} );

	describe( "when not denying access on a model's public property it", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( {
					"@hitchy.odem.model.Foo.list": "*",
					"@hitchy.odem.model.Foo.read": "*",
					"@hitchy.odem.model.Foo.write": "*",
					"@hitchy.odem.model.Foo.create": "*",
				} ),
			}
		} ) );

		it( "accepts to filter items by the private property", async() => {
			await create();

			( await ctx.get( "/api/foo?q=first:eq:foo" ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );
		} );

		it( "delivers the public property in listed items", async() => {
			await create();

			const res = await ctx.get( "/api/foo" );
			res.statusCode.should.equal( 200 );
			res.data.items.should.be.an.Array().with.length( 1 );
			res.data.items[0].should.have.property( "first" );
		} );

		it( "delivers the public property on reading an item", async() => {
			await create();

			const res = await ctx.get( "/api/foo/" + ctx.fooId );
			res.statusCode.should.equal( 200 );
			res.data.should.have.property( "first" );
		} );

		it( "considers public property on creating an item", async() => {
			await create();

			const first = await ctx.post( "/api/foo", { first: "created" } );
			first.statusCode.should.equal( 201 );

			const second = await ctx.post( "/api/foo/", { first: "created" } );
			second.statusCode.should.equal( 201 );

			const readFirst = await ctx.get( "/api/foo/" + first.data.uuid );
			readFirst.statusCode.should.equal( 200 );
			readFirst.data.first.should.equal( "created" );

			const readSecond = await ctx.get( "/api/foo/" + second.data.uuid );
			readSecond.statusCode.should.equal( 200 );
			readSecond.data.first.should.equal( "created" );
		} );

		it( "considers public property on replacing an item", async() => {
			await create();

			const write = await ctx.put( "/api/foo/" + ctx.fooId, { first: "replaced" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "replaced" );

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "replaced" );
		} );

		it( "considers public property on patching an item", async() => {
			await create();

			const write = await ctx.patch( "/api/foo/" + ctx.fooId, { first: "patched" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "patched" );

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "patched" );
		} );
	} );

	describe( "when trying to deny access on a model's public property it", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"api/model/foo.js": `export default ${JSON.stringify( modelFoo )};`,
				...authzConfig( {
					"@hitchy.odem.model.Foo.list": "*",
					"@hitchy.odem.model.Foo.read": "*",
					"@hitchy.odem.model.Foo.write": "*",
					"@hitchy.odem.model.Foo.create": "*",
					"@hitchy.odem.model.Foo.property.first.list": "-*",
					"@hitchy.odem.model.Foo.property.first.read": "-*",
					"@hitchy.odem.model.Foo.property.first.write": "-*",
					"@hitchy.odem.model.Foo.property.first.create": "-*",
				} ),
			}
		} ) );

		it( "still accepts to filter items by the private property", async() => {
			await create();

			( await ctx.get( "/api/foo?q=first:eq:foo" ) ).statusCode.should.equal( 200 );

			( await ctx.get( "/api/foo?query=" + encodeURIComponent( JSON.stringify( {
				or: [
					{ eq: { first: "foo" } },
					{ eq: { first: "bar" } }
				]
			} ) ) ) ).statusCode.should.equal( 200 );
		} );

		it( "still delivers the public property in listed items", async() => {
			await create();

			const res = await ctx.get( "/api/foo" );
			res.statusCode.should.equal( 200 );
			res.data.items.should.be.an.Array().with.length( 1 );
			res.data.items[0].should.have.property( "first" );
		} );

		it( "still delivers the public property on reading an item", async() => {
			await create();

			const res = await ctx.get( "/api/foo/" + ctx.fooId );
			res.statusCode.should.equal( 200 );
			res.data.should.have.property( "first" );
		} );

		it( "still considers public property on creating an item", async() => {
			await create();

			const first = await ctx.post( "/api/foo", { first: "created" } );
			first.statusCode.should.equal( 201 );

			const second = await ctx.post( "/api/foo/", { first: "created" } );
			second.statusCode.should.equal( 201 );

			const readFirst = await ctx.get( "/api/foo/" + first.data.uuid );
			readFirst.statusCode.should.equal( 200 );
			readFirst.data.first.should.equal( "created" );

			const readSecond = await ctx.get( "/api/foo/" + second.data.uuid );
			readSecond.statusCode.should.equal( 200 );
			readSecond.data.first.should.equal( "created" );
		} );

		it( "still considers public property on replacing an item", async() => {
			await create();

			const write = await ctx.put( "/api/foo/" + ctx.fooId, { first: "replaced" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "replaced" );

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "replaced" );
		} );

		it( "still considers public property on patching an item", async() => {
			await create();

			const write = await ctx.patch( "/api/foo/" + ctx.fooId, { first: "patched" } );
			write.statusCode.should.equal( 200 );
			write.data.first.should.equal( "patched" );

			const read = await ctx.get( "/api/foo/" + ctx.fooId );
			read.statusCode.should.equal( 200 );
			read.data.first.should.equal( "patched" );
		} );
	} );
} );

describe( "Fetching schemata of declared models", () => {
	const ctx = {};

	const files = {
		"api/model/first.js": `export default { props: { name: {} } };`,
		"api/model/second.js": `export default { props: { name: {} }, options: { promote: "public" } };`,
		"api/model/third.js": `export default { props: { name: {} }, options: { promote: "protected" } };`,
		"api/model/fourth.js": `export default { props: { name: {} }, options: { promote: "protected" } };`,
		"api/model/fifth.js": `export default { props: { name: {} }, options: { promote: "private" } };`,
		"api/model/sixth.js": `export default { props: { name: {} }, options: { promote: "protected" } };`,
	};

	describe( "works like before in case @hitchy/plugin-auth is missing and thus", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			options: { dependencies: [ "odm", "odm-provider" ] },
			files: {
				...files,
			}
		} ) );

		it( "lists schemata of models lacking promote option", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "first" );
		} );

		it( "lists schemata of models with 'public' promote option", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "second" );
		} );

		it( "conceals schemata of models with 'protected' promote option due to lacking any requesting user", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.not.have.property( "third" );
		} );

		it( "conceals schemata of models with 'private' promote option", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.not.have.property( "fifth" );
		} );
	} );

	describe( "requires special access granted for promoting schemata in case @hitchy/plugin-auth is found without any custom configuration and thus", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				...files,
			}
		} ) );

		it( "completely rejects request for schemata of available models", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 403 );
		} );
	} );

	describe( "requires special access granted for promoting schemata in case @hitchy/plugin-auth is found with a fallback configuration and thus", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				...files,
				...authzConfig( {
					"@hitchy.odem": "*",
				} ),
			}
		} ) );

		it( "lists schemata of models lacking promote option", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "first" );
		} );

		it( "lists schemata of models with 'public' promote option", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "second" );
		} );

		it( "lists schemata of models with 'protected' promote option due to implicitly granting access", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "third" );
		} );

		it( "conceals schemata of models with 'private' promote option", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.not.have.property( "fifth" );
		} );
	} );

	describe( "requires special access granted for promoting schemata in case @hitchy/plugin-auth is found with a proper configuration and thus", () => {
		afterEach( Test.after( ctx ) );
		beforeEach( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				...files,
				...authzConfig( {
					"@hitchy.odem.schema": "*",
					"@hitchy.odem.model": "*",
					"@hitchy.odem.model.First": "-*",
					"@hitchy.odem.model.Second": "-*",
					"@hitchy.odem.model.Third": "-*",
					"@hitchy.odem.model.Fifth": "*",
					"@hitchy.odem.model.Sixth": "-*",
					"@hitchy.odem.model.Sixth.promote": "*",
				} ),
			}
		} ) );

		it( "still lists schemata of models lacking promote option even though authorization has been declined explicitly", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "first" );
		} );

		it( "still lists schemata of models with 'public' promote option even though authorization has been declined explicitly", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "second" );
		} );

		it( "conceals schemata of models with 'protected' promote option due to revoking authorization from all users", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.not.have.property( "third" );
		} );

		it( "lists schemata of models with 'protected' promote option if authorization has not been revoked explicitly", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "fourth" );
		} );

		it( "still conceals schemata of models with 'private' promote option even though authorization has been granted explicitly", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.not.have.property( "fifth" );
		} );

		it( "lists schemata of models with 'protected' promote option if authorization has been revoked on the model, but granted on the promotion", async() => {
			const schemata = await ctx.get( "/api/.schema" );

			schemata.statusCode.should.equal( 200 );
			schemata.data.should.have.property( "sixth" );
		} );
	} );
} );

