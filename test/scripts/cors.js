/* eslint-disable max-nested-callbacks */
import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

const Methods = [ "DELETE", "GET", "HEAD", "PATCH", "POST", "PUT" ];
const Headers = [
	"Accept", "Accept-Language", "Authorization", "Content-Language",
	"Content-Type", "DNT", "Expect", "If-Match", "If-Modified-Since",
	"If-None-Match", "If-Range", "If-Unmodified-Since"
];

describe( "CORS preflight request", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: "../project",
	} ) );


	const tests = [
		{
			url: "/api/.schema",
			expected: {
				methods: ["GET"],
				headers: Headers,
			},
		},
		{
			url: "/api/mixed/.schema",
			expected: {
				methods: ["GET"],
				headers: Headers,
			},
		},
		{
			url: "/api/mixed",
			expected: {
				methods: [ "GET", "HEAD", "POST" ],
				headers: Headers,
			},
		},
		{
			url: "/api/mixed/12345678-1234-1234-1234-123456789012",
			expected: {
				methods: [ "DELETE", "GET", "HEAD", "PATCH", "PUT" ],
				headers: Headers,
			},
		},
	];

	tests.forEach( ( { url, expected: { methods: expectedMethods, headers: expectedHeaders } } ) => {
		const unacceptedMethods = Methods.filter( method => expectedMethods.indexOf( method ) < 0 );
		const unacceptedHeaders = Headers.filter( header => expectedHeaders.indexOf( header ) < 0 );

		describe( `for ${url}`, () => {
			it( "succeeds", () => {
				return ctx.options( url, {
					origin: "http://foo.bar"
				} )
					.then( res => {
						res.should.have.status( 200 );

						res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
						res.headers.should.not.have.property( "access-control-allow-methods" );
						res.headers.should.not.have.property( "access-control-allow-headers" );

						res.body.should.be.empty();
					} );
			} );

			expectedMethods.forEach( method => {
				it( `confirms accepted request method ${method}`, () => {
					return ctx.options( url, {
						origin: "http://foo.bar",
						"access-control-request-method": method,
					} )
						.then( res => {
							res.should.have.status( 200 );

							res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
							res.headers.should.have.property( "access-control-allow-methods" ).which.is.equal( method );
							res.headers.should.not.have.property( "access-control-allow-headers" );

							res.body.should.be.empty();
						} );
				} );
			} );

			unacceptedMethods.forEach( method => {
				it( `rejects unaccepted request method ${method}`, () => {
					return ctx.options( url, {
						origin: "http://foo.bar",
						"access-control-request-method": method,
					} )
						.then( res => {
							res.should.have.status( 200 );

							res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
							res.headers.should.have.property( "access-control-allow-methods" ).which.is.equal( expectedMethods.join( "," ) );
							res.headers.should.not.have.property( "access-control-allow-headers" );

							res.body.should.be.empty();
						} );
				} );
			} );

			it( "lists all accepted methods if requested one is not supported", () => {
				return ctx.options( url, {
					origin: "http://foo.bar",
					"access-control-request-method": "INVALID",
				} )
					.then( res => {
						res.should.have.status( 200 );

						res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
						res.headers.should.have.property( "access-control-allow-methods" ).which.is.equal( expectedMethods.join( "," ) );
						res.headers.should.not.have.property( "access-control-allow-headers" );

						res.body.should.be.empty();
					} );
			} );

			expectedHeaders.forEach( header => {
				it( `validates accepted request header ${header} omitting any unsupported header`, () => {
					return ctx.options( url, {
						origin: "http://foo.bar",
						"access-control-request-headers": `cookie,${header},pragma`,
					} )
						.then( res => {
							res.should.have.status( 200 );

							res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
							res.headers.should.not.have.property( "access-control-allow-methods" );
							res.headers.should.have.property( "access-control-allow-headers" ).which.is.equal( header );

							res.body.should.be.empty();
						} );
				} );
			} );

			unacceptedHeaders.forEach( header => {
				it( `rejects unaccepted request header ${header}`, () => {
					return ctx.options( url, {
						origin: "http://foo.bar",
						"access-control-request-headers": `content-type,${header},If-Match`
					} )
						.then( res => {
							res.should.have.status( 200 );

							res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
							res.headers.should.have.property( "access-control-allow-methods" ).which.is.equal( "Content-Type,If-Match" );
							res.headers.should.not.have.property( "access-control-allow-headers" );

							res.body.should.be.empty();
						} );
				} );
			} );

			it( "lists all accepted headers if neither requested header is accepted", () => {
				return ctx.options( url, {
					origin: "http://foo.bar",
					"access-control-request-headers": "cookie,pragma",
				} )
					.then( res => {
						res.should.have.status( 200 );

						res.headers.should.have.property( "access-control-allow-origin" ).which.is.equal( "http://foo.bar" );
						res.headers.should.not.have.property( "access-control-allow-methods" );
						res.headers.should.have.property( "access-control-allow-headers" ).which.is.equal( expectedHeaders.join( "," ) );

						res.body.should.be.empty();
					} );
			} );
		} );
	} );
} );
