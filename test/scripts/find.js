import { describe, it, xit, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "A simple data model", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: "../project",
	} ) );


	it( "can be populated with record", () => {
		return ctx.post( "/api/simple", {
			card: "13",
			user: "55f5365c-e1e7-4018-8051-dfae5a276dde",
		} )
			.then( res => {
				res.should.have.status( 201 ).and.be.json();
			} );
	} );

	it( "lists created record now", () => {
		return ctx.get( "/api/simple" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().and.has.length( 1 );

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty();
				res.data.items[0].card.should.equal( "13" );
				res.data.items[0].user.should.equal( "55f5365c-e1e7-4018-8051-dfae5a276dde" );
			} );
	} );

	it( "lists record when searching by card using simple query", () => {
		return ctx.get( "/api/simple?q=card:eq:13" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().and.has.length( 1 );

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty();
				res.data.items[0].card.should.equal( "13" );
				res.data.items[0].user.should.equal( "55f5365c-e1e7-4018-8051-dfae5a276dde" );
			} );
	} );

	it( "lists record when searching by card using extended query", () => {
		return ctx.get( "/api/simple?query=" + encodeURIComponent( JSON.stringify( { eq: { card: 13 } } ) ) )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().and.has.length( 1 );

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty();
				res.data.items[0].card.should.equal( "13" );
				res.data.items[0].user.should.equal( "55f5365c-e1e7-4018-8051-dfae5a276dde" );
			} );
	} );

	// not yet supported by node.js
	xit( "lists record when searching by card using HTTP QUERY method", () => {
		return ctx.request( "QUERY", "/api/simple", { eq: { card: 13 } } )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().and.has.length( 1 );

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty();
				res.data.items[0].card.should.equal( "13" );
				res.data.items[0].user.should.equal( "55f5365c-e1e7-4018-8051-dfae5a276dde" );
			} );
	} );
} );
