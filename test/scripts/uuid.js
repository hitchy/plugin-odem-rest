import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "model containing a UUID", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		files: {
			"api/model/uuid.js": `export default { props: { someUuid: { type: "uuid" } } };`,
		}
	} ) );

	it( "is exposed", () => {
		return ctx.get( "/api/uuid" )
			.then( res => {
				res.should.have.status( 200 );
			} );
	} );

	it( "does not have any record initially", () => {
		return ctx.get( "/api/uuid" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.empty();
			} );
	} );

	it( "provides number of records on demand, too", () => {
		return ctx.get( "/api/uuid", { "x-count": "1" } )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 2 ).and.has.properties( "items", "count" );
				res.data.items.should.be.an.Array().which.is.empty();
				res.data.count.should.be.a.Number().and.equal( 0 );
				res.headers.should.have.property( "x-count" ).which.is.a.String().and.is.equal( "0" );
			} );
	} );

	it( "is creating new record", () => {
		return ctx.post( "/api/uuid", { someUuid: "12345678-1234-1234-1234-123456789012" } )
			.then( res => {
				res.should.have.status( 201 ).and.be.json();
			} );
	} );

	it( "lists created record now", () => {
		return ctx.get( "/api/uuid" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty().and.is.not.equal( "12345678-1234-1234-1234-123456789012" );
				res.data.items[0].someUuid.should.equal( "12345678-1234-1234-1234-123456789012" );
			} );
	} );

	it( "provides updated number of records on demand", () => {
		return ctx.get( "/api/uuid", { "x-count": "1" } )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 2 ).and.has.properties( "items", "count" );
				res.data.items.should.be.an.Array().which.is.not.empty();
				res.data.count.should.be.a.Number().and.equal( 1 );
				res.headers.should.have.property( "x-count" ).which.is.a.String().and.is.equal( "1" );
			} );
	} );

	it( "updates previously created record", () => {
		return ctx.get( "/api/uuid" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				const uuid = res.data.items[0].uuid;

				return ctx.put( "/api/uuid/" + uuid, { someUuid: "98765432-abcd-abcd-abcd-098765432109" } )
					.then( res2 => {
						res2.should.have.status( 200 ).and.be.json();

						return ctx.get( "/api/uuid" )
							.then( res3 => {
								res3.should.have.status( 200 ).and.be.json();
								res3.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

								res3.data.items[0].should.have.property( "uuid" ).which.is.equal( uuid );
								res3.data.items[0].someUuid.should.be.equal( "98765432-abcd-abcd-abcd-098765432109" );
							} );
					} );
			} );
	} );
} );
