import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import Should from "should";
import "should-http";

const Test = await SDT( Core );

describe( "entries can be added", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: "../project",
	} ) );


	it( "is creating new record", () => {
		return ctx.post( "/api/mixed", {
			myStringProp: "entry no. 00",
			myIndexedStringProp: "entry no. 00",
			myIntegerProp: 0,
		} )
			.then( res => {
				res.should.have.status( 201 ).and.be.json();
			} );
	} );

	it( "lists created record now", () => {
		return ctx.get( "/api/mixed" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty();
				res.data.items[0].myStringProp.should.equal( "entry no. 00" );
			} );
	} );

	it( `is creating 30 new records`, () => {
		const Promises = new Array( 30 );

		for ( let index = 1, length = 30; index < length; index++ ) {
			Promises[index] = ctx.post( "/api/mixed", {
				myStringProp: `entry no. ${String( "0" + index ).slice( -2 )}`,
				myIndexedStringProp: `entry no. ${String( "0" + index ).slice( -2 )}`,
				myNullableStringProp: index % 2 ? `entry no. ${String( "0" + index ).slice( -2 )}` : null,
				myIndexedNullableStringProp: index % 2 ? `entry no. ${String( "0" + index ).slice( -2 )}` : null,
				myIntegerProp: index % 2 ? index : null,
			} )
				.then( res => {
					res.should.have.status( 201 ).and.be.json();
				} );
		}

		return Promise.all( Promises );
	} );

	it( "lists created record now", () => {
		return ctx.get( "/api/mixed" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				res.data.items[0].should.have.property( "uuid" ).which.is.a.String().which.is.not.empty();
				res.data.items.length.should.equal( 30 );
			} );
	} );

	it( "sorts list ascendingly on demand", () => {
		return ctx.get( "/api/mixed?sortBy=myStringProp" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				const { items } = res.data;
				for ( let i = 1, length = items.length; i < length; i++ ) {
					items[i].myStringProp.should.be.greaterThan( items[i - 1].myStringProp );
				}
			} );
	} );

	it( "sorts list descendingly on demand", () => {
		return ctx.get( "/api/mixed?sortBy=myStringProp&descending=true" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				const { items } = res.data;
				for ( let i = 1, length = items.length; i < length; i++ ) {
					items[i].myStringProp.should.be.lessThan( items[i - 1].myStringProp );
				}
			} );
	} );

	it( "creates another record lacking property used to sort by before", () => {
		return ctx.post( "/api/mixed" )
			.then( res => {
				res.should.have.status( 201 ).and.be.json();
			} );
	} );

	it( "sorts records providing value for sorting ascendingly and appends those missing related value", () => {
		return ctx.get( "/api/mixed?sortBy=myStringProp" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				const { items } = res.data;
				for ( let i = 1, length = items.length - 1; i < length; i++ ) {
					items[i].myStringProp.should.be.greaterThan( items[i - 1].myStringProp );
				}

				( items[items.length - 1].myStringProp == null ).should.be.true();
			} );
	} );

	it( "sorts records providing value for sorting descendingly and appends those missing related value", () => {
		return ctx.get( "/api/mixed?sortBy=myStringProp&descending=1" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.is.not.empty();

				const { items } = res.data;

				for ( let i = 1, length = items.length - 1; i < length; i++ ) {
					items[i].myStringProp.should.be.lessThan( items[i - 1].myStringProp );
				}

				( items[items.length - 1].myStringProp == null ).should.be.true();
			} );
	} );

	it( `lists correct number of entries if limit is used`, () => {
		const Promises = new Array( 31 );

		for ( let limit = 0; limit <= 31; limit++ ) {
			Promises[limit] = ctx.get( `/api/mixed?limit=${limit}` )
				.then( res => {
					res.should.have.status( 200 ).and.be.json();
					res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array();

					res.data.items.length.should.be.equal( limit );
				} );
		}

		return Promise.all( Promises );
	} );

	it( `lists correct number of entries if limit is used in sorted list`, () => {
		const Promises = new Array( 31 );
		for ( let limit = 0; limit <= 31; limit++ ) {
			Promises[limit] = ctx.get( `/api/mixed?limit=${limit}&sortBy=myStringProp` )
				.then( res => {
					res.should.have.status( 200 ).and.be.json();
					res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array();

					res.data.items.length.should.be.equal( limit );
				} );
		}
		return Promise.all( Promises );
	} );

	it( `lists the correct items if offset is used`, () => {
		return ctx.get( "/api/mixed?sortBy=myStringProp" )
			.then( re => {
				const fullList = re.data.items;
				const length = fullList.length;
				const Promises = new Array( length );
				for ( let offset = 0; offset < length; offset++ ) {
					Promises[offset] = ctx.get( `/api/mixed?sortBy=myStringProp&offset=${offset}` )
						.then( res => {
							res.should.have.status( 200 ).and.be.json();
							res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array();

							Should( res.data.items[0].myStringProp ).be.equal( fullList[offset].myStringProp );
							res.data.items[0].uuid.should.be.equal( fullList[offset].uuid );
						} );
				}
				return Promise.all( Promises );
			} );
	} );

	it( `lists the correct items if offset and limit is used`, () => {
		return ctx.get( "/api/mixed?sortBy=myStringProp" )
			.then( re => {
				const fullList = re.data.items;
				const length = fullList.length;
				const Promises = new Array( length );
				for ( let offset = 0; offset < length - 5; offset++ ) {
					Promises[offset] = ctx.get( `/api/mixed?sortBy=myStringProp&offset=${offset}&limit=5` )
						.then( res => {
							res.should.have.status( 200 ).and.be.json();
							res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 5 );

							res.data.items[0].myStringProp.should.be.equal( fullList[offset].myStringProp );
							res.data.items[res.data.items.length - 1].myStringProp.should.be.equal( fullList[offset + 4].myStringProp );
							res.data.items[0].uuid.should.be.equal( fullList[offset].uuid );
						} );
				}
				return Promise.all( Promises );
			} );
	} );

	it( `fetches items with property equal given value querying "prop:eq:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:eq:entry%20no.%2005" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 1 );
			} );
	} );

	it( `fetches items with property less than given value querying "prop:lt:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:lt:entry%20no.%2005" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 5 );
			} );
	} );

	it( `fetches items with property less than or equal given value querying "prop:lte:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:lte:entry%20no.%2005" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 6 );
			} );
	} );

	it( `fetches items with property greater than given value querying "prop:gt:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:gt:entry%20no.%2005" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 24 );
			} );
	} );

	it( `fetches items with property greater than or equal given value querying "prop:gte:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:gte:entry%20no.%2005" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 25 );
			} );
	} );

	it( `fetches items with property between two given values querying "prop:between:value:value"`, () => {
		return ctx.get( "/api/mixed?q=myIndexedStringProp:between:entry%20no.%2005:entry%20no.%2011" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 7 );
			} );
	} );

	it( `fetches items with property unset querying "prop:null"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:null" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array()
					.which.has.length( 0 + 1 ); // one extra item has been POSTed w/o any properties before

				return ctx.get( "/api/mixed?q=myIndexedStringProp:null" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array().which.has.length( 0 + 1 ); // including that extra item mentioned above

				return ctx.get( "/api/mixed?q=myNullableStringProp:null" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array().which.has.length( 15 + 1 ); // including that extra item mentioned above

				return ctx.get( "/api/mixed?q=myIndexedNullableStringProp:null" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array().which.has.length( 15 + 1 ); // including that extra item mentioned above
			} );
	} );

	it( `fetches items with property unset querying "prop:notnull"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:notnull" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array()
					.which.has.length( 30 );

				return ctx.get( "/api/mixed?q=myIndexedStringProp:notnull" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array().which.has.length( 30 );

				return ctx.get( "/api/mixed?q=myNullableStringProp:notnull" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array().which.has.length( 15 );

				return ctx.get( "/api/mixed?q=myIndexedNullableStringProp:notnull" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object()
					.which.has.size( 1 )
					.and.has.property( "items" )
					.which.is.an.Array().which.has.length( 15 );
			} );
	} );

	it( `supports slicing when fetching items with property equal given value when querying "prop:eq:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:eq:entry%20no.%2005&offset=0" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 1 );

				return ctx.get( "/api/mixed?q=myStringProp:eq:entry%20no.%2005&offset=5" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 0 );

				return ctx.get( "/api/mixed?q=myStringProp:eq:entry%20no.%2005&offset=0&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 1 );

				return ctx.get( "/api/mixed?q=myStringProp:eq:entry%20no.%2005&offset=5&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 0 );
			} );
	} );

	it( `supports slicing when fetching items with property less than given value when querying "prop:lt:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:lt:entry%20no.%2005&offset=0" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 5 );

				return ctx.get( "/api/mixed?q=myStringProp:lt:entry%20no.%2005&offset=5" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 0 );

				return ctx.get( "/api/mixed?q=myStringProp:lt:entry%20no.%2005&offset=0&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );

				return ctx.get( "/api/mixed?q=myStringProp:lt:entry%20no.%2005&offset=5&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 0 );
			} );
	} );

	it( `supports slicing when fetching items with property less than or equal given value when querying "prop:lte:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:lte:entry%20no.%2005&offset=0" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 6 );

				return ctx.get( "/api/mixed?q=myStringProp:lte:entry%20no.%2005&offset=5" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 1 );

				return ctx.get( "/api/mixed?q=myStringProp:lte:entry%20no.%2005&offset=0&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );

				return ctx.get( "/api/mixed?q=myStringProp:lte:entry%20no.%2005&offset=5&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 1 );
			} );
	} );

	it( `supports slicing when fetching items with property greater than given value when querying "prop:gt:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:gt:entry%20no.%2005&offset=0" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 24 );

				return ctx.get( "/api/mixed?q=myStringProp:gt:entry%20no.%2005&offset=5" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 19 );

				return ctx.get( "/api/mixed?q=myStringProp:gt:entry%20no.%2005&offset=0&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );

				return ctx.get( "/api/mixed?q=myStringProp:gt:entry%20no.%2005&offset=5&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );
			} );
	} );

	it( `supports slicing when fetching items with property greater than or equal given value when querying "prop:gte:value"`, () => {
		return ctx.get( "/api/mixed?q=myStringProp:gte:entry%20no.%2005&offset=0" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 25 );

				return ctx.get( "/api/mixed?q=myStringProp:gte:entry%20no.%2005&offset=5" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 20 );

				return ctx.get( "/api/mixed?q=myStringProp:lte:entry%20no.%2005&offset=0&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );

				return ctx.get( "/api/mixed?q=myStringProp:gte:entry%20no.%2005&offset=5&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );
			} );
	} );

	it( `supports slicing when fetching items with property between two given values when querying "prop:between:value:value"`, () => {
		return ctx.get( "/api/mixed?q=myIndexedStringProp:between:entry%20no.%2005:entry%20no.%2011&offset=0" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 7 );

				return ctx.get( "/api/mixed?q=myIndexedStringProp:between:entry%20no.%2005:entry%20no.%2011&offset=5" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 2 );

				return ctx.get( "/api/mixed?q=myIndexedStringProp:between:entry%20no.%2005:entry%20no.%2011&offset=0&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 3 );

				return ctx.get( "/api/mixed?q=myIndexedStringProp:between:entry%20no.%2005:entry%20no.%2011&offset=5&limit=3" );
			} )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Object().which.has.size( 1 ).and.has.property( "items" ).which.is.an.Array().which.has.length( 2 );
			} );
	} );
} );
